function init() {
	initStars()
	computeAge()
	insertSystemInfo()
}

function initStars() {

	const colors = [
		'#999','#9999ac','#ac9999','#acac99',
		'#bbb','#bbbbcf','#cfbbbb','#cfcfbb',
		'#eee','#e5e5ff','#ffe5e5','#ffffe5',
		'#fff'
	]
	const len = colors.length

	const canvas = document.getElementById('stars')
	const css = getComputedStyle(canvas)

	const w = canvas.width  = styleValue(css.getPropertyValue('width'))
	const h = canvas.height = styleValue(css.getPropertyValue('height'))

	// if CSS hasn't been loaded yet
	// (w and h contains NaN)
	const err = [w,h].some(n => isNaN(n))
	if (err) {
		setTimeout(init, 200)
		return
	}

	const ctx = canvas.getContext('2d')
	const radius = 3
	const circle = 2 * Math.PI

	ctx.clearRect(0, 0, w, h)
	for (let star = 0; star < 200; star++) {

		let color = Math.floor(Math.random() * len)
		ctx.fillStyle = colors[color]

		ctx.beginPath()
		ctx.arc(
			Math.round(Math.random() * w),
			Math.round(Math.random() * h),
			radius, 0, circle, false
		)
		ctx.fill()
	}
}

function computeAge() {

	const birthday = new Date(2009, 7, 13, 12, 30)
	const timedelta = Date.now() - birthday.getTime()

	/*
		The line below is equivalent to:
			const age = timedelta / 1000 / 60 / 60 / 24 / 365.25
		where:
			1000: convert milliseconds -> seconds
			60: secs -> minutes
			60: minutes -> hours
			24: hours -> days
			365.25: average count of days in a year:
				(366 + (365 * 3)) / 4 = 365.25
	*/
	const age = timedelta / (1000 * 60 * 60 * 24 * 365.25)

	document.getElementById('age-js').textContent = Math.floor(age)
}

function insertSystemInfo() {

	const inxi = document.getElementById('inxi')

	let xhr = new XMLHttpRequest()
	xhr.open('GET', '/space/inxi.txt')

	xhr.onreadystatechange = () => {
		if (xhr.readyState != xhr.DONE) return
		if (xhr.status != 200) return
		inxi.textContent = xhr.response
	}
	
	xhr.send()
}
