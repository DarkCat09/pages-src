const body = document.body

function copyLink(/** @type {Event} */ ev) {

	/** @type {HTMLSpanElement} */
	let elem = ev.currentTarget

	navigator.clipboard.writeText(elem.dataset.link)

	let wrapper = elem.parentElement.parentElement.parentElement
	let msg = wrapper.querySelector('.copied')

	msg.classList.add(...fadeAnimIn)

	setTimeout(() => {

		msg.classList.remove(...fadeAnimIn)

		msg.classList.add('show')
		msg.classList.add(...fadeAnimOut)

		setTimeout(() => {
			msg.classList.remove(...fadeAnimOut)
			msg.classList.remove(...fadeAnimIn)
		}, 600)

	}, 1500)
}

function styleValue(/** @type {string} */ prop) {
	return prop.replace('px', '') * 1
}
