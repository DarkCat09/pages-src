/** @type {HTMLDivElement} */
const menu = document.querySelector('.about-menu')

/** @type {HTMLLIElement} */
const move = document.querySelector('.about-menu li#move')

/** @type {HTMLDivElement} */
const context = document.querySelector('.context-menu')


var contextShown = false
var contextTimeout

var menuDragging = false
var mousePos = {x: 0, y: 0}


function moveMenu(/** @type {MouseEvent} */ ev) {
	switch (ev.type) {

		case 'mousedown':
			menuDragging = true
			mousePos.y = ev.clientY - styleValue(menu.style.top)
			mousePos.x = ev.clientX - styleValue(menu.style.left)
			menu.classList.add('dragging')
			ev.preventDefault()
			break

		case 'mouseup':
			menuDragging = false
			menu.classList.remove('dragging')
			break

		case 'mousemove':
			if (menuDragging) {
				let top  = ev.clientY - mousePos.y
				let left = ev.clientX - mousePos.x
				menu.style.top  = `${top}px`
				menu.style.left = `${left}px`
			}
			break
	}
}

function contextMenu(/** @type {MouseEvent} */ ev) {

	clearTimeout(contextTimeout)
	context.classList.remove(...bounceAnimOut)
	
	context.style.top  = `${ev.clientY + 5}px`
	context.style.left = `${ev.clientX + 5}px`

	context.classList.add(...bounceAnimIn)
	contextShown = true

	ev.preventDefault()
	return false
}

function hideContextMenu(/** @type {MouseEvent} */ ev) {

	if (!contextShown) return
	if (ev && ev.button != 0) return

	context.classList.remove(...bounceAnimIn)

	context.classList.add('show')
	context.classList.add(...bounceAnimOut)
	contextShown = false

	// animation-duration: 0.6s;
	contextTimeout = setTimeout(() => {
		context.classList.remove('show')
		context.classList.remove(...bounceAnimOut)
	}, 600)
}
