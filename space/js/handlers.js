context.addEventListener('mousedown', ev => ev.stopPropagation())

space.addEventListener('mousedown', ev => ev.preventDefault())
space.addEventListener('contextmenu', contextMenu)

document.querySelectorAll('.copylink>.profile-link').forEach(
	elem => elem.addEventListener('click', copyLink)
)

document.querySelectorAll('li.change-img>.li-wrapper').forEach(
	elem => elem.addEventListener('click', changePicture)
)

document.querySelector('li.change-speed input').addEventListener('input', changeSpeed)
document.querySelector('li.change-speed input').addEventListener('mousedown', ev => ev.stopPropagation())
document.querySelector('li.land-to').addEventListener('click', landToEarth)

move.addEventListener('mousedown', moveMenu)
move.addEventListener('mouseup', moveMenu)
move.addEventListener('mousemove', moveMenu)
body.addEventListener('mousemove', moveMenu)
body.addEventListener('mouseup', moveMenu)
body.addEventListener('mousedown', hideContextMenu)
