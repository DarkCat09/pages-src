const fadeAnimIn = [
	'show',
	'animated',
	'tdFadeIn',
]

const fadeAnimOut = [
	'animated',
    'tdFadeOut',
]


const bounceAnimIn = [
    'show',
    'animated',
    'tdExpandInBounce',
]

const bounceAnimOut = [
    'animated',
    'tdShrinkOutBounce',
]
