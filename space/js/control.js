/** @type {HTMLImageElement} */
const spaceObj = document.querySelector('#space-obj')

/** @type {HTMLDivElement} */
const space = document.querySelector('.space')


function changePicture(/** @type {Event} */ ev) {
	spaceObj.src = ev.target.dataset.img || 'img/earth.gif'
	hideContextMenu()
}

function changeSpeed(/** @type {Event} */ ev) {
	spaceObj.style.animationDuration = `${1200 - ev.target.value}s`
}

function landToEarth() {
	hideContextMenu()
}
