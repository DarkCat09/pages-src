System:
  Kernel: 5.19.1-3-MANJARO arch: x86_64 bits: 64 compiler: gcc v: 12.1.1
  Desktop: KDE Plasma v: 5.24.6 Distro: Manjaro Linux base: Arch Linux
CPU:
  Info: 6-core model: Intel Core i5-10400 bits: 64 type: MT MCP
  arch: Comet Lake rev: 3 cache: L1: 384 KiB L2: 1.5 MiB L3: 12 MiB
Graphics:
  Device-1: Intel CometLake-S GT2 [UHD Graphics 630] vendor: ASUSTeK
    driver: i915 v: kernel arch: Gen-9.5 bus-ID: 00:02.0
  Display: x11 server: X.Org v: 21.1.4 driver: X: loaded: modesetting
    gpu: i915 resolution: 1920x1080~60Hz
  OpenGL: renderer: Mesa Intel UHD Graphics 630 (CML GT2) v: 4.6 Mesa
  22.1.6 direct render: Yes
Drives:
  Local Storage: total: 1.36 TiB used: 180.51 GiB (12.9%)
  ID-1: /dev/nvme0n1 vendor: Western Digital model: WD Blue SN570 500GB
    size: 465.76 GiB temp: 37.9 C
  ID-2: /dev/sda vendor: Western Digital model: WD10EZEX-00BBHA0
    size: 931.51 GiB
Info:
  Processes: 291 Uptime: 1h 37m Memory: 30.21 GiB used: 4.49 GiB (14.9%)
  Init: systemd Compilers: gcc: 12.1.1 clang: 14.0.6 Packages: 1479
  Shell: Zsh v: 5.9 inxi: 3.3.21
